var randomstring = require("randomstring");

function Job(username){
    this.username = username;
}

Job.prototype.setJobObject = function(jobType, trade, jobSubtype, boatName, boatInfo, jobDesc, jobDate, location){
    this.type = jobType
    this.trade = trade
    this.subtype = jobSubtype
    this.boatName = boatName
    this.boatInfo = boatInfo
    this.jobDesc = jobDesc
    this.jobDate = jobDate
    this.location = location

}

Job.prototype.createType = function(jobType, jobSubtype){
    return 'job-' + jobType + '-' + jobSubtype + '-' + randomstring.generate(4);
}

module.exports = {
    Job
};