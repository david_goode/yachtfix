function User(username, type) {
    this.username = username;
    this.type = type;
}

User.prototype.setDetails = function (email, firstName, lastName, phoneNumber) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.email = email;
}

User.prototype.setOwnerJobs = function (jobs) {
    this.jobs = jobs
}

User.prototype.setActiveJobs = function (jobs) {
    this.activeJobs = jobs
}

User.prototype.setDBPreferences = function (pref) {
    this.preferences = pref
}

User.prototype.setProfileImageUrl = function (url) {
    this.profileImageUrl = url
}

//below is the most disgusting code i have written and im not proud.
//no idea how to set all the preferences depending on inputs. 

User.prototype.setPreferences = function (pref) {
    var preferences = {};
    console.log('prefernces::::::', pref);
    //engineer set

        if(pref.servicing == 'servicing'){
            preferences.servicing = true
        }else{
            preferences.servicing = false
        }
        if(pref.electrician == 'electrician'){
            preferences.electrician = true
        }else{
            preferences.electrician = false
        }
        if(pref.systems == 'systems' ){
            preferences.systems = true
        }else{
            preferences.systems = false
        }
        if(pref.hydraulic == 'hydraulic'){
            preferences.hydraulic = true
        }else{
            preferences.hydraulic = false
        }

    // SailsRigging set
        if(pref.sails == 'sails'){
            preferences.sails = true
        }else{
            preferences.sails = false
        }
        if(pref.rigginghardware == 'rigginghardware'){
            preferences.rigginghardware = true
        }else{
            preferences.rigginghardware = false
        }
        if(pref.rigging == 'rigging'){
            preferences.rigging = true
        }else{
            preferences.rigging = false
        }

    //Shipwright set

        if(pref.fibreglass == 'fibreglass'){
            preferences.fibreglass = true
        }else{
            preferences.fibreglass = false
        }
        if(pref.steel == 'steel'){
            preferences.steel = true
        }else{
            preferences.steel = false
        }
        if(pref.timber == 'timber'){
            preferences.timber = true
        }else{
            preferences.timber = false
        }
        if(pref.painting == 'painting'){
            preferences.painting = true
        }else{
            preferences.painting = false
        }
        if(pref.hull == 'hull'){
            preferences.hull =true
        }else{
            preferences.hull = false
        }
        if(pref.joinery == 'joinery'){
            preferences.joinery = true
        }else{
            preferences.joinery = false
        }

    //Plumber set
 
        if(pref.desalination == 'desalination'){
            preferences.desalination = true
        }else{
            preferences.desalination = false
        }
        if(pref.plumbing == 'plumbing'){
            preferences.plumbing = true
        }else{
            preferences.plumbing = false
        }
        if(pref.gas == 'gas'){
            preferences.gas = true
        }else{
            preferences.gas = false
        }
        if(pref.aircon == 'aircon'){
            preferences.aircon = true
        }else{
            preferences.aircon = false
        }
        if(pref.refrigeration == 'refrigeration'){
            preferences.refrigeration = true
        }else{
            preferences.refrigeration = false
        }

    //Detailing set
        if(pref.diving == 'diving'){
            preferences.diving = true
        }else{
            preferences.diving = false
        }
        if(pref.polishing == 'polishing'){
            preferences.polishing = true
        }else{
            preferences.polishing = false
        }
        if(pref.teak == 'teak'){
            preferences.teak = true
        }else{
            preferences.teak = false
        }
        if(pref.upholstery == 'upholstery'){
            preferences.upholstery = true
        }else{
            preferences.upholstery = false
        }
    this.preferences = preferences
}

User.prototype.echo = function (jobs) {
    console.log('hello?')
} 


module.exports = {
    User,
}
