var globaljob;
var globallocation;

function jobParams(type, subtype, jobDesc, location, boatName, boatInfo, jobDate, extraDetails) {
  var job = {
    type: type,
    trade: globaljob,
    subtype: subtype,
    location: location,
    jobDesc: jobDesc,
    boatName: boatName,
    boatInfo: boatInfo,
    jobDate: jobDate,
    extraDetails: extraDetails
  }
  var jsonstring = JSON.stringify(job)
  return jsonstring
}

function submitJob() {
  var boatName = document.getElementById('boatName').value;
  var boatInfo = document.getElementById('boatInfo').value;
  var subtype;
  var buttons = document.getElementsByName('subtype')
  for (var i = 0; i < buttons.length; i++) {
    if (buttons[i].checked) {
      subtype = buttons[i].value
    }
  }
  var jobDesc = document.getElementById('jobDesc').value;
  var jobDate = document.getElementById('jobDate').value;

  var ajaxjob = jobParams(globaljob, subtype, jobDesc, globallocation, boatName, boatInfo, jobDate, 'placeholder')
  submitAJAX(ajaxjob, 'newjob/submit', '/user')
}

function submitTradieLocation() {
  var jsonstring = JSON.stringify(globallocation);
  submitAJAX(jsonstring, '/user/tradie/updateLocation', '/user')
}

function submitSignUp() {
  var captchaResponse;
  grecaptcha.ready(function () {
    grecaptcha.execute('6LeVjc8ZAAAAAC7GDOpuql1Gw3z18k-JaPMnM8r9', { action: 'submit' }).then(function (token) {
      captchaResponse = token

      console.log('captchaResponse', captchaResponse)
      var signUpForm = {
        firstName: document.getElementById('firstName').value,
        lastName: document.getElementById('lastName').value,
        email: document.getElementById('email').value,
        password: document.getElementById('password').value,
        confirmPassword: document.getElementById('confirmPassword').value,
        type: document.querySelector('input[name="type"]:checked').value,
        captcha: captchaResponse
      }
      console.log(signUpForm);
      var JSONsignUpForm = JSON.stringify(signUpForm);
      submitAJAX(JSONsignUpForm, '/user/register/submit', '/user')
    });
  });
}

function submitAJAX(data, url, action) {
  $.ajax({
    type: 'POST',
    url: url,
    data: data,
    dataType: 'json',
    contentType: 'application/json',
    error: function (jqXHR, exception) {
      var msg = 'an AJAX error has occured. please try again, or contact us to help fix the issue.';
      if (jqXHR.status === 0) {
        msg = 'Not connect.\n Verify Network.';
      } else if (jqXHR.status == 404) {
        msg = 'Requested page not found. [404]';
      } else if (jqXHR.status == 500) {
        msg = 'Internal Server Error [500].';
      } else if (exception === 'parsererror') {
        msg = 'Requested JSON parse failed.';
      } else if (exception === 'timeout') {
        msg = 'Time out error.';
      } else if (exception === 'abort') {
        msg = 'Ajax request aborted.';
      } else {
        msg = 'Uncaught Error.\n' + jqXHR.responseText;
      }
      alert(msg)
    }, success: function (data) {
      if (data.username == 'bot') {
        //window.location.replace('/')
      } else {
        window.location.replace(action)
      }
    }
  });
}

function setGlobalJob(jobType) {
  globaljob = jobType;
}

function setGlobalLocation(location) {
  globallocation = location;
}

function setGlobalLocation(location) {
  globallocation = location;
}

function setGlobalExtraJobDetails(details) {
  globalExtraJobDetails = details;
}

function getGlobalJob() {
  return globaljob
}

function getGlobalLocation() {
  return globallocation
}

function testDoc() {
  console.log('js doc is working')
} 