const express = require('express');
const app = express();
const session = require('express-session');
const passport = require('passport');
const bodyParser = require('body-parser')
app.set('view engine', 'ejs');
const homeRouter = require('./routes/home')
const userRouter = require('./routes/user');
const jobRouter = require('./routes/job');

//Express session
app.use(session({
  //secret needs to be an environmental variable
    secret: 'G0G0G@dg3tR3dIs69',
    saveUninitialized: false,
    resave: false
  }));

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(passport.initialize());
app.use(passport.session());
//static pages 
app.use(express.static('public'));

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});

/* CONNECTING TO ROUTE FILES */

app.use('/', homeRouter);
app.use('/user', userRouter);
app.use('/job', jobRouter);
/*-------------------------------------*/


/*  STARTING APPLICATION ON LOCALHOST  */
const port = process.env.PORT || 80;
app.listen(port, () => console.log(`Listening on port ${port}...`));

/* -------------------------------------- */