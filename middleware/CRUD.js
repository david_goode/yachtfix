var AWS = require('aws-sdk');
const DBparams = require('../model/DBparams.js');
AWS.config.update(DBparams.getDatabaseConfig());
var docClient = new AWS.DynamoDB.DocumentClient();
var s3 = new AWS.S3();

function addDatabaseItem(params){
	return new Promise((resolve, reject) => {
		docClient.put(params, (err, data) => {
			if (err) {
				console.log('addItem err:', err)
				reject(err)
			}
			else {
				console.log('addItem success', data)
				resolve('success')
			}
		});
	});
};

function getDatabaseItem(params){
	return new Promise((resolve, reject) => {
		docClient.get(params,(err, data) => {
			if(err){
				console.log('getItem err: ', err);
				reject(err);
			}else{
				console.log('Get Item data', data);
				resolve(data);
			}
		})
	});
}

function queryDatabaseItems(params){
	return new Promise((resolve, reject) => {
		docClient.query(params,(err, data) => {
			if(err){
				console.log('Query err:', err);
				reject(err);
			}else{
				//console.log('Query data', data);
				resolve(data);
			}
		}).promise();
	});
}

function deleteDatabaseItem(params){
	docClient.delete(params, function(err, data) {
		if (err) {
			console.error("Unable to delete item. Error JSON:", JSON.stringify(err, null, 2));
		} else {
			console.log("DeleteItem succeeded:", JSON.stringify(data, null, 2));
		}
	});
};

function updateDatabaseItem(params){
    docClient.update(params, function(err, data) {
		if (err) {
			console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
		} else {
			console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
		}
	});
};

module.exports = {
	addDatabaseItem,
	queryDatabaseItems,
	getDatabaseItem,
	deleteDatabaseItem,
	updateDatabaseItem
}