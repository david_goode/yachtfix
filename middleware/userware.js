const database = require('../middleware/CRUD')
var userParams = require('../model/userParams');
var jobParams = require('../model/jobParams');
const userJS = require('../public/js/User.js')
const jobware = require('../middleware/jobware')
var geohash = require('ngeohash');

async function getUser(username, type){
    try{
        var DBuser = await database.getDatabaseItem(userParams.getUserParam(username, type));
        var DBuser = DBuser.Item;
        const user = new userJS.User(username, type);
        user.setDetails(DBuser.email, DBuser.firstName, DBuser.lastName, DBuser.phone)
        if(user.type = 'owner'){
            var ownerJobs = await jobware.getOwnerJobs(username)
            user.setOwnerJobs(ownerJobs);
        } 
        if(user.type = 'tradie'){
            var preferences = await database.getDatabaseItem(userParams.getUserParam(user.username, 'preferences'))
            var activeJobs = await database.queryDatabaseItems(jobParams.queryJobsByStatusParam('accepted:'+user.username));
            user.setActiveJobs(activeJobs.Items)
            user.setDBPreferences(preferences.Item)
        }
        user.setProfileImageUrl('https://marine-tasker.s3-ap-southeast-2.amazonaws.com/' + username + '/ProfileImage')
        return user;

    }catch(error){
        console.log('an error occured in userware.js getUser()', error)
    }
}

async function setTradiePreferences(preferences, username){
    try{
        const tradie = new userJS.User(username, 'tradie')
        console.log(preferences)
        tradie.setPreferences(preferences);
        database.updateDatabaseItem(userParams.updatePreferences(username, tradie.preferences))
        return tradie;
    }catch(error){
        console.log('error in userware.js setTradiePreferences()', error)
    }
}

async function updateTradieLocation(username, lat, lng){
    try{
        var hashedlocation = geohash.encode_int(lat, lng);
        console.log(hashedlocation)
        //325233 - sydney harbour
        //325234 - botany bay
        //326415 - newcastle
        //return a bunch of tradesmen in the same cells.
        //then filter with .distance1(hash1, hash2) to check distance and filter. (To be coded)
        database.updateDatabaseItem(userParams.updatePreferenceLocation(username, hashedlocation))
    }catch(error){
        console.log('error in userware.js updateTradieLocation()', error)
    }
}

async function updateOwnerSettings(){
    try{
        //database call to pass updated or new settings object //what options?
    }catch(error){
        console.log('catch block in userware updateOwnerSettings()',error);
    }
}

async function updateTradieSettings(){
    try{
        //database call to pass updated or new settings object //what options?
    }catch(error){
        console.log('catch block in userware updateOwnerSettings()',error);
    }
}

//will return all active jobs matching tradie preferences. 
async function getTradieRecommendedJobs(username){
    var tradiePrefs = await database.getDatabaseItem(userParams.getUserParam(username, 'preferences'));
    //query for job preferences returning in a set location. use hashed location and all parameters from preferences.
    var hashedLocation = tradiePrefs.Item.hashedLocation;
    var maxHash = hashedLocation + 150000000000000
    var minHash = hashedLocation - 150000000000000

    var jobs = await database.queryDatabaseItems(jobParams.queryJobsByStatusHashedLocationParam(minHash, maxHash, 'new'))
    console.log("jobs",jobs)
    var returnJobs = [];
    
    jobs.Items.forEach((job)=>{
        console.log((job.subtype).toString())
        if(tradiePrefs.Item[(job.subtype).toString()] == true){
            returnJobs.push(job);
        }
    }) 
    return returnJobs;
}

module.exports = {
    getUser,
    setTradiePreferences,
    updateTradieLocation,
    updateOwnerSettings,
    updateTradieSettings,
    getTradieRecommendedJobs
}