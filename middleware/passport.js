const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
const database = require('../middleware/CRUD')
var userParams = require('../model/userParams');
const bcrypt = require('bcryptjs');
const userJS = require('../public/js/User.js');
const register = require('../middleware/register')

module.exports = passport.use(new LocalStrategy({
    usernameField: 'email',
		passwordField: 'password' },
    function(email, password, done) {
      try{
      database.queryDatabaseItems(userParams.queryByEmail(email)).then((object)=>{
        if (!object.Items[0]) {
          console.log('username not found in DB')
          return done(null, false, { message: 'Incorrect username.' });
        }
          var DBuser = object.Items[0]
          bcrypt.compare(password, object.Items[0].password).then((result)=>{
            if (result == true){
              const user = {username: object.Items[0].username , type: object.Items[0].type}
              return done(null, user, {message: 'YAY IT WORKED'});
            } else {
              console.log('wrong password')
              return done(null, false, { message: 'Incorrect password.' });
            }
          })
        })
      }catch(error){
        console.log('passport error', error)
      }
    }
  ));

module.exports = passport.use(new FacebookStrategy({
    clientID: "681006782530343",
    clientSecret: process.env.FACEBOOK_SECRET,
    callbackURL: process.env.FACEBOOK_CALLBACK,
    profileFields: ['id', 'displayName', 'email', 'first_name', 'last_name', 'birthday']
  },
  async function(accessToken, refreshToken, profile, cb) {
    console.log('profile', profile)
    database.queryDatabaseItems(userParams.queryByEmail(profile.emails[0].value))
	  var finduser =  await database.queryDatabaseItems(userParams.queryByEmail(profile.emails[0].value));
	  if(!finduser.Items[0]){
      var newUser = {username: profile.id , type: 'newUser', firstName: profile.name.givenName, lastName: profile.name.familyName, email: profile.emails[0].value}
		  console.log('newUser:', newUser)
		  return cb(null, newUser);
	  }else{
      const returnUser = {username: finduser.Items[0].username , type: finduser.Items[0].type}
      console.log('user found:', returnUser)
      return cb(null, returnUser);
    }
  }
));

