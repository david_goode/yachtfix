var AWS = require('aws-sdk');
const DBparams = require('../model/DBparams.js');
AWS.config.update(DBparams.getEmailConfig());
var email = new AWS.SES();
const template = require('../model/emailTemplate.js');

async function sendPasswordResetLink(userEmail) {
  var passwordLink = await getResetLink(userEmail);
  var params = {
    Destination: {
      ToAddresses: [
        userEmail,
      ]
    },
    Message: {
      Body: {
        Text: {
          Charset: "UTF-8",
          Data: "Please reset your password with this link. : " + passwordLink
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: 'Your password has been reset ~ marine-tasker.com'
      }
    },
    Source: 'notify@marine-tasker.com',
  };
  var confirmation = await email.sendEmail(params).promise();
  //TODO: error checking here for password send
  return confirmation;
}

async function sendContactUs(req) {
  var params = {
    Destination: {
      ToAddresses: [
        'david@marine-tasker.com',
      ]
    },
    Message: {
      Body: {
        Text: {
          Charset: "UTF-8",
          Data: template.getContactUsEmail(req.body.message, req.body.firstName, req.body.email)
        },
        Html:{
          Data: template.getContactUsEmail(req.body.message, req.body.firstName, req.body.email)
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: 'Contact Us : from '+req.body.firstName
      }
    },
    Source: 'david@marine-tasker.com',
  };
  var confirmation = await email.sendEmail(params).promise();
  return confirmation;
}

async function sendResetPasswordLink(resetLink, emailTo) {
  var params = {
    Destination: {
      ToAddresses: [
        emailTo,
      ]
    },
    Message: {
      Body: {
        Text: {
          Charset: "UTF-8",
          Data: template.getPasswordReset(resetLink)
        },
        Html:{
          Data: template.getPasswordReset(resetLink)
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: 'Password Reset Link'
      }
    },
    Source: 'notify@marine-tasker.com',
  };
  var confirmation = await email.sendEmail(params).promise();
  console.log(confirmation);
  return confirmation;
}

async function sendContactUs(req) {
  var params = {
    Destination: {
      ToAddresses: [
        'david@marine-tasker.com',
      ]
    },
    Message: {
      Body: {
        Text: {
          Charset: "UTF-8",
          Data: template.getContactUsEmail(req.body.message, req.body.firstName, req.body.email)
        },
        Html:{
          Data: template.getContactUsEmail(req.body.message, req.body.firstName, req.body.email)
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: 'Contact Us : from '+req.body.firstName
      }
    },
    Source: 'david@marine-tasker.com',
  };
  var confirmation = await email.sendEmail(params).promise();
  return confirmation;
}

async function sendJobNotification(job, tradie, owner) {
  var params = {
    Destination: {
      ToAddresses: [
        //tradie.email
        'davidgoode229@hotmail.com',
      ]
    },
    Message: {
      Body: {
        Text: {
          Charset: "UTF-8",
          Data: 'Emailed not rendered correctly. '
        },
        Html: {
          Data: template.getJobNotifyEmail(job, tradie, owner)
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: 'Marine Tasker ~ JOB NOTIFICATION'
      }
    },
    Source: 'notify@marine-tasker.com',
  };
  var confirmation = await email.sendEmail(params).promise();
  console.log('confirmation', confirmation);
  return confirmation;
}

async function sendWelcomeNotification(userEmail, type, firstName) {

  var params = {
    Destination: {
      ToAddresses: [
        userEmail,
      ]
    },
    Message: {
      Body: {
        Text: {
          Charset: "UTF-8",
          Data: 'Emailed not rendered correctly.'
        },
        Html: {
          Data: template.getWelcomeEmail(firstName, type)
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: 'Marine Tasker ~ WELCOME'
      }
    },
    Source: 'notify@marine-tasker.com',
  };
  var confirmation = await email.sendEmail(params).promise();
  console.log('confirmation', confirmation);
  return confirmation;
}


module.exports = {
  sendPasswordResetLink,
  sendContactUs,
  sendJobNotification,
  sendWelcomeNotification,
  sendResetPasswordLink
}