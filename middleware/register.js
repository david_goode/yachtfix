const database = require('../middleware/CRUD')
var userParams = require('../model/userParams');
var DBParams = require('../model/DBparams');
var randomstring = require("randomstring");
const bcrypt = require('bcryptjs');
var userParams = require('../model/userParams');
const email = require('../middleware/email');
const request = require('request');

async function newUser(req){
    try{
        console.log(req.body)
        var captcha = await verifyCapture(req.body.captcha)
        if(captcha == false){
            console.log('captcha false')
            return {username: 'bot', type:'bot'}
        }else{
            var checkEmail = await database.queryDatabaseItems(userParams.queryByEmail(req.body.email))
            if(checkEmail.Count > 0){
                console.log('the email already exists in the database.')
                return {username: false};
            }else{
                if(req.body.password != req.body.confirmPassword){
                    console.log('passwords do not match');
                }else{
                    var uniqueUsername = req.body.email.split('@')[0] + "-" + randomstring.generate(5);
                    await bcrypt.hash(req.body.password, 10).then(function(passwordHash) {
                        database.addDatabaseItem(userParams.addUserParam(uniqueUsername, passwordHash, req.body.email, req.body.type, req.body.firstName, req.body.lastName, req.body.phoneNumber));
                        //hidden to stop testing spam
                        email.sendWelcomeNotification(req.body.email, req.body.type, req.body.firstName);
                        console.log('new user added');
                        
                    });
                    return {username: uniqueUsername, type: req.body.type};
                }
            }
        }
    }catch(error){
        console.log('an error occured in register/newUser()', error)
    }
}

async function newFacebookUser(user, type){
    try{
        database.addDatabaseItem(userParams.addFacebookUserParam(user, type));
        console.log('new facebook user');
        email.sendWelcomeNotification(user.email,type, user.firstName);
        return true
    }catch(error){
        console.log('caught in new facebook user', error);
    }
}

async function verifyCapture(token){
    return new Promise((resolve, reject) => {
        var captchaURL = 'https://www.google.com/recaptcha/api/siteverify?secret=' + process.env.CAPTCHA_KEY + '&response=' + token;
        request(captchaURL,function(error,response,body) {
            body = JSON.parse(body);
            console.log('captchaBODY',body);
            if(body.success !== true) {
              reject('captcha fail')
            } else if(body.score < 0.8){
                reject('bot detection: low captcha score')
            } else{
                console.log('not a bot')
                resolve(true)
            }
          });
    });
}

async function resetPasswordRequest(req){
    try{
        //query database for email and return object.
        var checkEmail = await database.queryDatabaseItems(userParams.queryByEmail(req.body.email))
            if(checkEmail.Count == 0){
                console.log('email not found in database.')
                return false;
            }else{
                console.log(checkEmail);
                var resetLink = 'https://marine-tasker.com/user/resetPassword/' + checkEmail.Items[0].type + '/'+checkEmail.Items[0].username + '/' + checkEmail.Items[0].password
                console.log('resetLink', resetLink)
                await email.sendResetPasswordLink(resetLink, checkEmail.Items[0].email);
                return true;
            }
    }catch(error){
        console.log('catch block in userware updateOwnerSettings()',error);
    }
}

async function updateNewPassword(req){
    try{
        //check passwords match
        if(req.body.password != req.body.passwordCheck){
            console.log('passwords dont match')
            return false;
        } else {
            console.log(req.body)
            var user = await database.getDatabaseItem(userParams.getUserParam(req.body.username, req.body.type))
            //check url password hash matches for security
            if(!user.Item || user.Item.password != req.body.passwordHash){
                console.log('security check failed.')
                return false;
            }else{
                //bcrypt new password and update
                bcrypt.hash(req.body.password, 10).then(function(passwordHash) {
                    database.updateDatabaseItem(userParams.updateNewPassword(req.body.username, req.body.type, passwordHash))
                    console.log('passed check and password updated')
                });
                return true;
            }
        }

    }catch(error){
        console.log('catch block in userware newPassword()',error);
    }
}

module.exports = {
    newUser,
    newFacebookUser,
    verifyCapture,
    resetPasswordRequest,
    updateNewPassword
}