const database = require('../middleware/CRUD')
const JobJS = require('../public/js/Job.js')
const jobParams = require('../model/jobParams')
var geohash = require('ngeohash');
const userParams = require('../model/userParams');
const geolib = require('geolib');
const email = require('../middleware/email')

//this function hashes the lat long location using geohash.(smaller and smaller squares on worlds surface.)
//currently hardcoded returning tradesmen within approximate area.
//filter tradies function can handle more 
async function findTradies(job){

    var hashedLocation = geohash.encode_int(job.lat, job.lng);
    var maxHash = hashedLocation + 150000000000000
    var minHash = hashedLocation - 150000000000000
    var tradies = await database.queryDatabaseItems(userParams.queryTradiesByHashedLocationParam(minHash, maxHash))
    var filteredTradies = filterTradies(job, tradies.Items)
    notifyTradies(filteredTradies, job) 
}
function filterTradies(job, tradies){
    //filter if tradie pref job is true. filter by distance that tradesman has selected (currently hardcoded distance)
    var checkjob = job.subtype
    var returnTradies = []
    tradies.forEach(tradie => {
        if(tradie[checkjob]){
            var tradieLocation = geohash.decode_int(tradie.hashedLocation)
            var distance = geolib.getDistance({latitude: tradieLocation.latitude, longitude: tradieLocation.longitude}, {latitude: job.lat, longitude: job.lng})
            if(distance < 100000){
                //this push has to be email addresses, not usernames, so emails can be instantly sent in next function.
                returnTradies.push(tradie.username);
            }
        }
    });
    return returnTradies
}

async function notifyTradies(filteredTradies, job){
    console.log('list of filtered tradesmen',filteredTradies);
    var databaseOwner = await database.getDatabaseItem(userParams.getUserParam(job.username, 'owner'))
    for(var i = 0; i < filteredTradies.length; i++){
        console.log('job to notify',job);
        var databaseTradie = await database.getDatabaseItem(userParams.getUserParam(filteredTradies[i], 'tradie'))
        email.sendJobNotification(job, databaseTradie.Item, databaseOwner.Item);
    }
    //for loop her to loop through the tradies and send them notifcation emails on the jobs
    
}

module.exports = {
    findTradies
}