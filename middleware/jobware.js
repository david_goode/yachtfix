const database = require('../middleware/CRUD')
const JobJS = require('../public/js/Job.js')
const jobParams = require('../model/jobParams')
const geohash = require('ngeohash');
const notify = require('../middleware/notify')

async function newJob(ajaxJob, username){
    try {
        var job = new JobJS.Job(username)
        var type = job.createType(ajaxJob.type, ajaxJob.subtype);
        console.log(ajaxJob)
        var hashedlocation = geohash.encode_int(ajaxJob.location.lat, ajaxJob.location.lng);
        console.log('hashed job location',hashedlocation)
        job.setJobObject(type, ajaxJob.type, ajaxJob.subtype, ajaxJob.boatName, ajaxJob.boatInfo, ajaxJob.jobDesc, ajaxJob.jobDate, hashedlocation)
        database.addDatabaseItem(jobParams.addJobParam(job));
        notify.findTradies(job);
        return job;
    } catch (error) {
        console.log('an error occured in jobware/newJob()', error)
    }
}

async function getOwnerJobs(username){
    var DBjobs = await database.queryDatabaseItems(jobParams.queryJobsByUsernameParam(username));
    var returnJobs = []
    DBjobs.Items.forEach(job =>{
        var JSjob = new JobJS.Job(username)
        JSjob.setJobObject(job.type, job.trade, job.subtype, job.boatName, job.boatInfo, job.jobDesc, job.jobDate, {lat: job.lat, lng: job.long})
        returnJobs.push(JSjob);
    })
    return returnJobs
}

function deleteJob(username, type){
    database.deleteDatabaseItem(jobParams.deleteJobParam(username, type));
}

async function getJob(username, type){
    var job = await database.getDatabaseItem(jobParams.getJobParam(username, type))
    return job.Item
}

async function acceptJob(jobUsername, jobType, tradieUsername){
    var status = 'accepted:'+ tradieUsername;
    await database.updateDatabaseItem(jobParams.updateJobStatusParam(jobUsername, jobType, status))
    return true;
}

async function cancelJob(req){

    var job =  await database.getDatabaseItem(jobParams.getJobParam(req.params.username, req.params.type))
    console.log('job returned', job.Item);
    if(job.Item.status.includes(req.user.username) || req.user.username == job.Item.username){
        database.updateDatabaseItem(jobParams.cancelJobParam(job.Item.username, job.Item.type, req.body.message));
        if(job.Item.status.includes(req.user.username)){
            //email tradie of cancellation if job has been accepted by them
            //notify.cancelJob(job.Item, req.user.username);
        }
        return true;
    } else{
        console.log('not allowed to cancel job')
        return false
    }
}

module.exports = {
    newJob,
    getOwnerJobs,
    deleteJob,
    getJob,
    acceptJob,
    cancelJob,
}