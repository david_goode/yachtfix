const tableName = process.env.DATABASE_TABLE;

/*GET*/

function getUserParam(username, type){
	return param = {
		TableName: tableName,
		Key: {
			username: username,
			type: type
		}
	};
}

/* ADD */

function addUserParam(username, password, email, type, firstName, lastName, phone){
	return param = {
		TableName: tableName,
		Item: {
            'username' : username,
			'email' : email,
			'password': password,
			'type' : type,
			'firstName' : firstName,
			'lastName' :  lastName,
			'phone' : phone,
			'lastLoggedIn' : Date.now(),
			'description' : 'placeholder'
		}
	}
}

function addFacebookUserParam(user, type){
	console.log('params user', user)
	return param = {
		TableName: tableName,
		Item: {
			'username' : user.username,
			'type' : type,
			'email' : user.email,
			'firstName' : user.firstName,
			'lastName' :  user.lastName,
			'lastLoggedIn' : Date.now(),
			'description' : 'placeholder'
		}
	}
}

/* QUERY */

function queryByEmail(email){
	return param = {
		TableName: tableName,
		IndexName: 'email-index',
		KeyConditionExpression: "#email = :email",
		ExpressionAttributeNames:{
			"#email": "email",
		},
		ExpressionAttributeValues: {
			":email" : email,
		}
	}
}

function queryTradiesByHashedLocationParam(minHash, maxHash){
	return param = {
		TableName: tableName,
		IndexName: 'type-hashedLocation-index',
		KeyConditionExpression: "#type = :type and #hashedLocation between :minHash and :maxHash",
		ExpressionAttributeNames:{
			"#type": "type",
			'#hashedLocation' : "hashedLocation"
		},
		ExpressionAttributeValues: {
			":maxHash" : maxHash,
			":minHash" : minHash,
			":type" : "preferences"
		}
	}
}

/* DELETE */


/* UPDATE */

function updatePreferences(username, preferences){ 
	return param = {
		TableName: tableName,
		Key:{
			username: username,
			type: 'preferences'
		},
		UpdateExpression: 'set servicing = :servicing, electrician = :electrician, systems = :systems, hydraulic = :hydraulic, sails = :sails, riggingHardware = :rigginghardware, rigging = :rigging, fibreglass = :fibreglass, steel = :steel, timber = :timber, painting = :painting, hull = :hull, joinery = :joinery, desalination = :desalination, plumbing = :plumbing, gas = :gas, aircon = :aircon, refrigeration = :refrigeration, diving = :diving, polishing = :polishing, teak = :teak, upholstery = :upholstery',
		ExpressionAttributeValues:{
			':servicing': preferences.servicing,
			':electrician' : preferences.electrician,
			':systems' : preferences.systems,
			':hydraulic' : preferences.hydraulic,
			':sails' : preferences.sails,
			':rigginghardware' : preferences.rigginghardware,
			':rigging' : preferences.rigging,
			':fibreglass' : preferences.fibreglass,
			':steel' : preferences.steel,
			':timber' : preferences.timber,
			':painting' : preferences.painting,
			':hull' : preferences.hull,
			':joinery' : preferences.joinery,
			':desalination' : preferences.desalination,
			':plumbing' : preferences.plumbing,
			':gas' : preferences.gas,
			':aircon' : preferences.aircon,
			':refrigeration' : preferences.refrigeration,
			':diving' : preferences.diving,
			':polishing' : preferences.polishing,
			':teak' : preferences.teak,
			':upholstery' : preferences.upholstery
		}
	}
}

function updatePreferenceLocation(username, hashedlocation){ 
	return param = {
		TableName: tableName,
		Key:{
			username: username,
			type: 'preferences'
		},
		UpdateExpression: 'set hashedLocation = :hashedlocation',
		ExpressionAttributeValues:{
			':hashedlocation':hashedlocation
		}
	}
}

function updateNewPassword(username, type, password){ 
	return param = {
		TableName: tableName,
		Key:{
			username: username,
			type: type
		},
		UpdateExpression: 'set password = :password',
		ExpressionAttributeValues:{
			':password': password
		}
	}
}

module.exports = {
	addUserParam,
	queryByEmail,
	getUserParam,
	updatePreferences,
	updatePreferenceLocation,
	queryTradiesByHashedLocationParam,
	addFacebookUserParam,
	updateNewPassword,
}