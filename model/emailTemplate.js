
function getJobNotifyEmail(job, tradie, owner){
    return  '<p>Hello ' + tradie.firstName + '</p> A <b>' + job.subtype + '</b> job has been requested in your area for the <b>' + job.jobDate + '</b>. <p>Google link to job location: <a href = \"https://www.google.com/maps?q=' + job.lat + ',' + job.lng + '&zoom=12\">google map link</a><p>Please contact the boat owner, <b>' + owner.firstName + '</b><br>, to request this job<br>Phone: PLACEHOLDER <br>Email: ' + owner.email + '</p><p>Once the job quote has been discussed and agreed with the owner, accept the job with this link: <a href = \"http://marine-tasker.com/job/' + job.username + '/' + job.type + '\"> Marine-Tasker job link</a></p><p>Kind Regards<br>Marine Tasker</p>'
}

function getWelcomeEmail(firstName, type){
    return '<p>Hello ' + firstName + '</p> <p> Welcome to Marine Tasker. You have signed up as a <b>' + type + '</b></p> <p>Kind Regards<br>Marine Tasker</p>'
}

function getContactUsEmail(message, firstname, email){
    return '<p>From: ' + email + '</p><p>Name: ' + firstname + '</p><p>Message: ' + message +'</p>'
}

function getPasswordReset(resetLink){
    return '<p>Hello</p><p>Your password has been requested to be reset, use this link to reset your password: <a href = \"' + resetLink + '\">Reset Password</a></p><p>Kind Regards<br>Marine Tasker</p>'
}

module.exports = {
    getJobNotifyEmail,
    getWelcomeEmail,
    getContactUsEmail,
    getPasswordReset
}