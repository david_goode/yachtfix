const tableName = process.env.DATABASE_TABLE;

/*GET*/

function getJobParam(username, type){
	return param = {
		TableName: tableName,
		Key: {
			username: username,
			type: type
		}
	};
}


/* ADD */

function addJobParam(job){
	return param = {
		TableName: tableName,
		Item: {
            'username' : job.username,
			'type' : job.type,
			'trade' : job.trade,
            'subtype' : job.subtype,
            'boatName' : job.boatName,
            'boatInfo' : job.boatInfo,
            'hashedLocation' : job.location,
            'jobDate' : job.jobDate,
            'jobDesc' : job.jobDesc,
            'status' : 'new'
		}
	}
}

/* QUERY */
function queryJobsByUsernameParam(username){
	return param = {
		TableName: tableName,
		KeyConditionExpression: "#username = :username and begins_with(#type, :job)",
		ExpressionAttributeNames:{
			"#username": "username",
			"#type" : "type"
		},
		ExpressionAttributeValues: {
			":username" : username,
			":job" : 'job'
		}
	}
}

function queryJobsByStatusParam(statusString){
	return param = {
		TableName: tableName,
		IndexName: 'status-index',
		KeyConditionExpression: "#status1 = :statusString",
		ExpressionAttributeNames:{
			'#status1' : "status"
		},
		ExpressionAttributeValues: {
			":statusString" : statusString,
		}
	}
}

function queryJobsByStatusHashedLocationParam(minHash, maxHash, statusString){
	return param = {
		TableName: tableName,
		IndexName: 'status-hashedLocation-index',
		KeyConditionExpression: "#status1 = :statusString and #hashedLocation between :minHash and :maxHash",
		ExpressionAttributeNames:{
			'#status1' : "status",
			'#hashedLocation' : "hashedLocation"
		},
		ExpressionAttributeValues: {
			":statusString" : statusString,
			":minHash" : minHash,
			":maxHash" : maxHash,
		}
	}
}

/* DELETE */

function deleteJobParam(username, type){
	return param = {
		TableName: tableName,
		Key: {
			username: username,
			type: type
		}
	};
}

/* UPDATE */
/*
function updateTradieCurrentJobs(username, type, listItem){
	return param = {
		TableName: tableName,
		Key:{
			username: username,
			type: type
		},
		UpdateExpression: 'set currentJobs = list_append(:element, currentJobs)',
		ExpressionAttributeValues:{
			':element': [listItem]
		},
	}
}*/

function updateJobStatusParam(jobUsername, jobType, status){
	return param = {
		TableName: tableName,
		Key:{
			username: jobUsername,
			type: jobType
		},
		UpdateExpression: 'set #sts = :newStatus',
		ExpressionAttributeNames:{
			'#sts' : "status"
		},
		ExpressionAttributeValues:{
			':newStatus': status
		}
	}
}

function cancelJobParam(jobUsername, jobType, message){
	return param = {
		TableName: tableName,
		Key:{
			username: jobUsername,
			type: jobType
		},
		UpdateExpression: 'set #sts = :newStatus, cancelMessage = :message',
		ExpressionAttributeNames:{
			'#sts' : "status"
		},
		ExpressionAttributeValues:{
			':newStatus': 'cancelled',
			':message' : 'placeholderrrrrrrr'
		}
	}
}

module.exports = {
    addJobParam,
	queryJobsByUsernameParam,
	deleteJobParam,
	getJobParam,
	updateJobStatusParam,
	queryJobsByStatusParam,
	queryJobsByStatusHashedLocationParam,
	cancelJobParam
}
