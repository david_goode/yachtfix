
function getDatabaseConfig(){
    return {
		  accessKeyId: process.env.ACCESS_ID,
		  secretAccessKey: process.env.ACCESS_KEY,
		  region: 'ap-southeast-2',
	  };
}


function getEmailConfig(){
    return {
		  accessKeyId: process.env.ACCESS_ID,
		  secretAccessKey: process.env.ACCESS_KEY,
		  region: 'us-west-2',
	  };
}

module.exports = {
    getDatabaseConfig,
	getEmailConfig,
}