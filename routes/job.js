const express = require('express');
const router = express();
var jobware = require('../middleware/jobware');
var notify = require('../middleware/notify');

router.get('/newjob', (req, res) => {
    if(!req.isAuthenticated()){
        res.send('please log in to post a job')
    }else{
        res.render('yf-views/newjob' , {loggedIn: req.isAuthenticated()});
    }
})

router.post('/newjob/submit', (req, res) => {
    jobware.newJob(req.body, req.user.username)
    res.send({data: 'success!'})
})

router.post('/delete/:username/:type', (req, res) => {
    if(req.isAuthenticated()){
        jobware.deleteJob(req.params.username, req.params.type);
    } else {
        res.send('not authorized, please log in')
    }
    res.redirect('/user')
})

router.get('/:username/:type', (req, res) => {
    jobware.getJob(req.params.username, req.params.type).then((returnedJob)=>{
        res.render('yf-views/job' , {loggedIn: req.isAuthenticated(), job: returnedJob});
    })
})

router.post('/accept/:username/:type', (req, res) => {
    if(req.isAuthenticated() && req.user.type == 'tradie'){
        jobware.acceptJob(req.params.username, req.params.type, req.user.username)
        res.redirect('/user')
    }else{
        res.send('not authorized, please log in as a tradie')
    }
})

router.post('/cancel/:username/:type', (req, res) => {
    if(req.isAuthenticated()){
        jobware.cancelJob(req).then((response)=>{
            if(response == false){
                res.send('something went wrong')
            }
        })
        res.redirect('/user')
    }else{
        res.send('not authorized, please log in')
    }
})


module.exports = router;