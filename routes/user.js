const express = require('express');
const router = express();
const userware = require('../middleware/userware')
const register = require('../middleware/register')

/* upload requirements. struggling to move code into middleware */
var paramDOC = require('../model/DBparams.js');
var AWS = require('aws-sdk');
AWS.config.update(paramDOC.getDatabaseConfig());
var s3 = new AWS.S3();
var multer = require('multer')
var multerS3 = require('multer-s3')


router.get('/', (req, res) => {
    if(!req.user){
        res.send('Error:  not logged in?');
    }
    if(req.user.type == 'owner'){
        res.redirect('/user/owner/' + req.user.username)
    }
    if(req.user.type == 'tradie'){
        res.redirect('/user/tradie/' + req.user.username)
    }
    if(req.user.type == 'newUser'){
        res.render('yf-views/selectType', {loggedIn: req.isAuthenticated()});
    }
})

// *** OWNER ROUTES ***

router.get('/owner/:username', (req, res) => {
    userware.getUser(req.params.username, 'owner').then((returnedUser)=>{
        //res.send(returnedUser);
        res.render('yf-views/owner', {owner: returnedUser, loggedIn: req.isAuthenticated()});
    })
})

// ** TRADIE ROUTES **

router.get('/tradie/:username', (req, res) => {
    userware.getUser(req.params.username, 'tradie').then((returnedUser)=>{
        console.log('returnedUser', returnedUser)
        res.render('yf-views/tradie', {tradie: returnedUser, loggedIn: req.isAuthenticated()});
    })
})

router.post('/tradie/updateLocation', (req, res) => {
    userware.updateTradieLocation(req.user.username, req.body.lat, req.body.lng).then((data)=>{
        res.send('location updated')
    })
})

router.post('/tradie/submitPreferences', (req, res) => {
    userware.setTradiePreferences(req.body, req.user.username).then((returnedUser)=>{
        res.redirect('/user/tradie/' + req.user.username);
        //res.send(returnedUser)
    })
})

router.get('/tradie/:username/recommendedJobs', (req, res) => {
    userware.getTradieRecommendedJobs(req.params.username).then((returnedJobs)=>{
        console.log('returnedJobs', returnedJobs)
        res.render('yf-views/jobs', {jobs: returnedJobs, loggedIn: req.isAuthenticated()});
    })
})

//send a reset password link to email
router.post('/forgotPasswordRequest', (req, res) => {
    register.resetPasswordRequest(req).then((returnedUser)=>{
        if(returnedUser == false){
            res.send('email not found in our database.')
        }
        res.send('A reset password link has been sent to your email. Please click the link sent and be directed to enter a new password.')
    })
})

//reset password form
router.get('/resetPassword/:type/:username/:passwordHash', (req, res) => {
    res.render('yf-views/resetPassword', {type: req.params.type, passwordHash: req.params.passwordHash, username:req.params.username, loggedIn:false})
})

router.post('/newPassword', (req, res) => {
    register.updateNewPassword(req).then((data)=>{
        if(data == false){
            res.send('password reset fail')
        } else{
            req.login({username: req.body.username, type: req.body.type}, (err)=>{
                res.redirect('/user')
            })
        }
    })
})

//still to be coded and tested
router.post('/submitAccountSettings', (req, res) => {
    if(req.user == 'owner'){
        userware.updateOwnerSettings().then((returnData)=>{
            res.redirect('/user');
        })
    }
    if(req.user == 'tradie'){
        userware.updateTradieSettings(req.body, req.user.username).then((returnData)=>{
            res.redirect('/user');
        })
    }
})

//route for facebook login when type on signup is unknown
router.post('/settype/:type', (req, res) => {
    console.log(req.user)
    register.newFacebookUser(req.user, req.params.type).then((data)=>{
        req.login({username: req.user.username, type: req.params.type}, (err)=>{
            res.redirect('/user')
        })
    }).catch((err)=>{
        console.log('settype err', err)
    })
})

router.get('/register', (req, res) => {
    res.render('yf-views/userRegistration', {loggedIn: req.isAuthenticated()});
})

router.post('/register/submit', (req, res) => {
    //result returns false if user exists, else it returns the new user id
    console.log('req body', req.body)
    register.newUser(req).then((data) => {
        console.log('redirect username', data);
        if(data.username == false){
            res.send({data: 'user exists in db already'})
            console.log('user already existing, needs to be handled better')
        } else if(data.username == 'bot'){
            console.log('bot detected');
            res.send({data: 'bot'});
        } else {
            req.login({username: data.username, type: data.type}, (err)=>{
                res.send({data: 'success!'})
            })
        }
    }).catch((error) =>{
        console.log('error from /user/register/submit', error);
    })

})

var uploadUserProfileImage = multer({
	storage: multerS3({
	  s3: s3,
	  bucket: 'marine-tasker',
	  acl: 'public-read',
	  metadata: function (req, file, cb) {
		  console.log('file:',file);
		cb(null, {fieldName: file.fieldname});
	  },
	  key: function (req, file, cb) {
		  console.log('uploading with key', req.user.username)
		cb(null, req.user.username+'/ProfileImage')
	  }
	})
  })

router.post('/submitProfileImage', uploadUserProfileImage.single('fileToUpload'), (req, res) => {
	console.log('redirected...');
	res.redirect('/user');
});

module.exports = router;