const express = require('express');
const router = express();
var passport = require('../middleware/passport');
var email = require('../middleware/email');
const jobJS = require('../public/js/Job.js');

router.get('/', (req, res) => {
  console.log('home page user session working for: ', req.user)
  console.log('is authenticated?', req.isAuthenticated());
    res.render('yf-views/home', {loggedIn: req.isAuthenticated(), captcha: process.env.CAPTCHA_FRONT});
})

router.post('/login/submit', passport.authenticate('local', { successRedirect: '../user', failureRedirect: '/'}), (req, res) => {
  //res.redirect('/')
  //res.redirect('../user/'+req.user.type+'/' + req.user.username);
});

router.get('/auth/facebook', passport.authenticate('facebook', { scope: 'email'}));

router.get('/auth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/', successRedirect: '../../user' }));

router.get('/login', (req, res) => {
  res.render('yf-views/login');
});

router.get('/logout', function(req, res){
  req.logout();
  res.redirect('/')
});

router.get('/err', (req, res) => {
  res.render('test/err');
});

router.get('/about', (req, res) => {
  console.log('home locals', res.locals.loggedIn)
  res.render('yf-views/about', {loggedIn: req.isAuthenticated(), captcha: process.env.CAPTCHA_FRONT});
});

router.get('/pricing', (req, res) => {
  res.render('yf-views/pricing', {loggedIn: req.isAuthenticated(), captcha: process.env.CAPTCHA_FRONT});
});

router.get('/payment', (req, res) => {
  res.render('yf-views/payment', {loggedIn: req.isAuthenticated(), captcha: process.env.CAPTCHA_FRONT, keyPublishable: 'pk_test_KsP8AnmbgaDSApwaXuMBjlem009nT9cwo7'});
});

router.post('/contact', (req, res) =>{
    email.sendContactUs(req);
    res.send('Thanks for your email. We will be in contact with you shortly.');
});


/* TESTING ROUTES */

router.get('/post-test', (req, res) => {
    res.render('test/post-test');
})

router.post('/post-test/submit', (req, res) => {
  //splitting data into lat and long for easier storage and calling.
  var answers = req.body.answers; 
  console.log(answers)
  res.send(answers);
})

router.get('/map', (req, res) => {
  res.render('test/map');
})

router.post('/map/marker', (req, res) => {
  //splitting data into lat and long for easier storage and calling.
  const newjob = new jobJS.Job('NEW JOB');
  newjob.setLocation(req.body.location);
  console.log(newjob.getLocation())
  //res.send(newjob)
  res.render('test/boatmap', {newjob});
})

module.exports = router